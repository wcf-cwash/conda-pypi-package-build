#!/bin/bash

# Prerequisites: you must have these installed in order for this script to work!
# conda install conda-build
# conda install anaconda-client
#
# Before running the script, make sure to login so the upload works properly
# anaconda login # provide your username and password you signed up for with anaconda.org
# 

# change the package name to the existing PyPi package you would like to build and adjust the Python versions
pkg='flask-restful'
array=( 3.5 3.6 3.7 3.8 3.9)

echo "Building conda package ..."
cd ~
conda skeleton pypi $pkg
cd $pkg
wget https://conda.io/docs/_downloads/build1.sh
wget https://conda.io/docs/_downloads/bld.bat
cd ~

# building conda packages
for i in "${array[@]}"
do
	conda-build --python $i $pkg
done

# convert package to other platforms
cd ~
platforms=( osx-64 osx-arm64 linux-64 win-64 )
find /opt/conda/conda-bld/linux-64/ -name *.tar.bz2 | while read file
do
    echo $file
    #conda convert --platform all $file  -o /opt/conda/conda-bld/
    for platform in "${platforms[@]}"
    do
       conda convert --platform $platform $file  -o /opt/conda/conda-bld/
    done
    
done

# upload packages to conda
find /opt/conda/conda-bld/ -name *.tar.bz2 | while read file
do
    echo $file
    anaconda upload $file
done

echo "Building conda package done!"